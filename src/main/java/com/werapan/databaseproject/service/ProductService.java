/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.werapan.databaseproject.service;

import com.werapan.databaseproject.dao.ProductDao;
import com.werapan.databaseproject.model.Product;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author patch
 */
public class ProductService {
      public Product getById(int id) {
        ProductDao productDao = new ProductDao();
        return productDao.get(id);
    }
    
    public ArrayList<Product> getProductOrderByName() {
        ProductDao productDao = new ProductDao();
        return (ArrayList<Product>) productDao.getAll(" product_name asc");
    }
    
    public List<Product> getProducts(){
        ProductDao productDao = new ProductDao();
        return productDao.getAll(" product_id asc");
    }
    

    public Product addNew(Product editedProduct) {
        ProductDao productDao = new ProductDao();
        return productDao.save(editedProduct);
    }

    public Product update(Product editedProduct) {
         if (editedProduct != null) {
            ProductDao proDao = new ProductDao();
            return proDao.update(editedProduct);
        } else {
            System.err.println("Cannot update a null product.");
            return null;
        }
    }

    public int delete(Product editedProduct) {
        ProductDao productDao = new ProductDao();
        return productDao.delete(editedProduct);
    }
}
